### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ a871c088-a8a6-41b5-970b-e2bd8cc59250
function mutate_(bag) 
    if rand() > 0.5
        if rand() > 0.5
            # mutate price
            bag = [(p[1], p[2], p[3]*.1 + p[3]) for p in bag]
        else
            # mutate weight
            bag = [(p[1], p[2]*.05 + p[2], p[3]) for p in bag]
        end
    end
    return bag
end;

# ╔═╡ 541c65b9-f821-4e58-9b94-d421847d26f9
function mutate_all(population, mutation_chance)
    for i in 1:length(population)
        if rand() < mutation_chance
            population[i] = mutate_(population[i])
        end
    end
end;

# ╔═╡ 3e34aa4d-d6f6-4ed7-8088-12ba692f1d40
begin
MAX_WEIGHT = 20
paintings = [("Salvator Mundi", 7, 450),
			 ("Interchange", 5, 300),
			 ("The Card Players", 6, 288),
			 ("Nafea Faa Ipoipo", 4, 229),
			 ("Number 17A", 5, 218),
			 ("Wasserschlangen II", 4, 204),
			 ("Violet, Green and Red", 6, 203),
			 ("Pendant portraits of Maerten Soolmans and Oopjen Coppit", 5, 197),
			 ("Les Femmes d'Alger", 4, 195),
			 ("Nu couché", 4, 186)];
end;

# ╔═╡ 5a33ad5c-7cec-41a3-8443-4b1554a70dde
function fitness(bag)
    return sum(abs(painting[2] - MAX_WEIGHT) * painting[3] for painting in bag)
end;

# ╔═╡ 5063d192-acbd-478f-af45-ebd75bcd6d4f
function select_parents(population)
    # selection 2 parents using TOURNAMENT selection
    length_pop = length(population)
    chosen_individuals = [population[rand(1:length_pop)] for _ in 1:length_pop ÷ 2]
    chosen_parents = sort(chosen_individuals, rev=true, by=fitness)[1:2]
    return chosen_parents
end;

# ╔═╡ f37b00fa-c26d-46c5-8ceb-bd4a8770b40d
function get_idx(parents)
    bag1, bag2 = parents
    weight_bag1 = sum(p[2] for p in bag1)
    weight_bag2 = sum(p[2] for p in bag1)

    len = min(length(bag1), length(bag2))

    for (i, (p1, p2)) in zip(1:len, zip(bag1, bag2))
        if ((weight_bag1 - p1[2]) + p2[2] <= MAX_WEIGHT && 
            (weight_bag2 - p2[2]) + p1[2] <= MAX_WEIGHT && 
            p1[1] !== p2[1])
            return i
        end
    end
    return nothing
end;

# ╔═╡ c0a2efd2-80c0-4099-8ca6-0010fc5e2892
function crossover(parents)
    bag1, bag2 = parents
    idx = get_idx(parents)

    if idx === nothing
        return parents
    end

    bag1[idx] = bag2[idx]
    bag2[idx] = bag1[idx]
    return [bag1, bag2]
end;

# ╔═╡ 8da8052b-b379-48e9-9af6-1dc6537b2578
function get_new_population(population, crossover_chance)
    # Reproduce and returns a new population
    newpop = []
    length_p = length(population)
    while length(newpop) < length_p
        parents = select_parents(population)
        parents = rand() < crossover_chance ? crossover(parents) : parents
        newpop = vcat(newpop, parents)
    end
    if length(newpop) > length_p
        pop!(newpop)
    end
    return newpop
end;

# ╔═╡ 8734f06a-ce71-4686-94b4-6113251e7d8b
function generic_algorithm(params)
    # returns the best individual
    population = params.population
    crossover_chance = params.crossover_chance
    mutation_chance = params.mutation_chance
    mutate = params.mutate
    N = params.N

    best_individual = sort(population, rev=true, by=fitness)[1]
    for _ in 1:N
        population = get_new_population(population, crossover_chance)

        mutate(population, mutation_chance)

        highest = sort(population, rev=true, by=fitness)[1]
        best_individual = fitness(highest) > fitness(best_individual) ? highest : best_individual
    end
    return (best=best_individual, score=fitness(best_individual))
end;

# ╔═╡ 0add316f-a5ea-4d3e-8550-84a1d85777b6
function get_bag()
    bag = []
    weight = 0
    i = 0
    while weight <= MAX_WEIGHT
        painting = paintings[rand(1:length(paintings))]
        if !(painting in bag) && weight + painting[2] <= MAX_WEIGHT
            push!(bag, painting)
            weight += painting[2]
        end
        if i == 20
            break
        end
        i += 1
    end
    return bag
end;

# ╔═╡ 42a0f686-d901-4573-aa6b-e7b4f1c28277
result = generic_algorithm((population=[get_bag() for _ in 1:20], N=100,                                           mutation_chance=.01, crossover_chance=.7, 
		                    mutate=mutate_all))

# ╔═╡ Cell order:
# ╠═5063d192-acbd-478f-af45-ebd75bcd6d4f
# ╠═8da8052b-b379-48e9-9af6-1dc6537b2578
# ╠═541c65b9-f821-4e58-9b94-d421847d26f9
# ╠═a871c088-a8a6-41b5-970b-e2bd8cc59250
# ╠═8734f06a-ce71-4686-94b4-6113251e7d8b
# ╠═5a33ad5c-7cec-41a3-8443-4b1554a70dde
# ╠═f37b00fa-c26d-46c5-8ceb-bd4a8770b40d
# ╠═c0a2efd2-80c0-4099-8ca6-0010fc5e2892
# ╠═0add316f-a5ea-4d3e-8550-84a1d85777b6
# ╠═3e34aa4d-d6f6-4ed7-8088-12ba692f1d40
# ╠═42a0f686-d901-4573-aa6b-e7b4f1c28277
