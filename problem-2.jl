### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ de8ebfb0-bf1f-4749-8f1e-71cf17208173
mutable struct CSP 
    variables::Array{}
    domains::Dict{String, Array{Int64}}
    constraint_network::Dict{String, Array{String}}
end

# ╔═╡ 5b049e9b-fa57-467f-9cb9-ac007beeff28
function is_consistent(var, assignments, csp=csp)
    for arc in csp.constraint_network[var]
		if haskey(assignments, arc) && assignments[arc] == assignments[var]
			return false
		end
    end
    return true 
end;

# ╔═╡ 657dff82-3262-4f9b-8780-ef58689a7e59
function undo(var, value, csp=csp)
    for neighbour in csp.constraint_network[var]
        push!(csp.domains[neighbour], value)
    end
end;

# ╔═╡ 610a4b28-6259-4bab-a58d-ee1afbe85db9
function forward_checking(variable, value, assignments, csp)
	
   # ----Forward-Cheking--------
    neighbours = [n for n in csp.constraint_network[variable] if !haskey(assignments, n)]
    for n in neighbours
        csp.domains[n] = filter!(val->val≠value, csp.domains[n])
    end

    # -----Propagation-----
    for n in neighbours
        if length(csp.domains[n]) == 1
            println("      Propagating on Neighbour: $n")
            forward_checking(n, csp.domains[n][1], assignments, csp)
        end
    end
    println("Finished Forward-Checking on: $variable")

    has_succeed = all(!isempty(csp.domains[n]) for n in neighbours)

    if !has_succeed
        println("-------------Failed--------------- assigning value=$value to Variable=$variable")
    else
        println("-------------Pass----------------- assigning value=$value to Variable=$variable")
        domains = csp.domains
        println("----------(new) Domains After ----------")
	for (k, v) in collect(csp.domains)
		println("$k = $v")
	end
	println("-------------------------------------")
    end
    println()

    return has_succeed
end;

# ╔═╡ 9f8493e2-00e1-44ca-a85f-341f9fb6c632
begin
	variables = ["X$n" for n=1:7]
	domains = Dict(var => [1, 2, 3, 4] for var in variables)
	domains["X3"] = [1]
	constraint_network = Dict("X1"=>["X2", "X3", "X4", "X5", "X6"],
						   "X2"=>["X1", "X5"],
						   "X3"=>["X1", "X4"],
						   "X4"=>["X1", "X3", "X5", "X6"],
						   "X5"=>["X1", "X2", "X4", "X6"],
						   "X6"=>["X1", "X4", "X5", "X7"],
						   "X7"=>["X6"])
   csp = CSP(variables, deepcopy(domains), constraint_network);	
end;

# ╔═╡ d524e47e-ddc6-11eb-35a9-a318a0c6cbf0
function backtracking_search(csp, assignments=Dict())
    if length(assignments) == length(csp.variables)
        return assignments
    end

	variable = setdiff(variables, keys(assignments))[1]
    for value in csp.domains[variable]
        new_assignments = copy(assignments)
        new_assignments[variable] = value

        if is_consistent(variable, new_assignments)
			 println("---------- Domains Before ----------")
			for (k, v) in collect(csp.domains)
				println("$k = $v")
			end
			println("-------------------------------------")
			println()
			n = csp.constraint_network[variable]
			println("Forward-Checking on: $variable's neighbours: $n")
            if forward_checking(variable, value, new_assignments, csp)
                result = backtracking_search(csp, new_assignments)
                if result ≠ nothing
                    return result
                end
            else
                undo(variable, value, csp) 
            end
        end
    end
end;

# ╔═╡ 65cb94ed-eccb-43db-80e4-443ae850ce31
solution = backtracking_search(deepcopy(csp))

# ╔═╡ Cell order:
# ╠═de8ebfb0-bf1f-4749-8f1e-71cf17208173
# ╠═5b049e9b-fa57-467f-9cb9-ac007beeff28
# ╠═657dff82-3262-4f9b-8780-ef58689a7e59
# ╠═610a4b28-6259-4bab-a58d-ee1afbe85db9
# ╠═d524e47e-ddc6-11eb-35a9-a318a0c6cbf0
# ╠═9f8493e2-00e1-44ca-a85f-341f9fb6c632
# ╠═65cb94ed-eccb-43db-80e4-443ae850ce31
